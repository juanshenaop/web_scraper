# -*- coding: utf-8 -*-
import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

PRODUCTION_ENVIROMENT = False

# Database connection.
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'database/Happylives.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}

# Enable protection agains - Cross-site Request Forgery (CSRF).
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for signing the data.
CSRF_SESSION_KEY = 'Ym6y46Ve6kjxrlyOa1omfVEyE36TGirTq2jvmKRl'

# Secret key for signing cookies.
SECRET_KEY = os.environ.get('SECRET_KEY') or 'D426tYYDqykFhiRCaHmVmCq9h8ct3kWneydmT6AC'

SECURITY_PASSWORD_SALT = '4juN0CbVeOdSWsw29BLFwzZnS8KxCOW3F3n0yY29'

SERVER_EMAIL = 'MAILGUN'
API_KEY = 'fe5542552b25eaf62f80a0a82f0bfe6a-059e099e-27f83328'
API_BASE_URL = 'https://api.mailgun.net/v3/send.happylives.co'
