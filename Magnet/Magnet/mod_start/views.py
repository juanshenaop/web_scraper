# -*- coding: utf-8 -*-
# Standard library imports
import os
import glob

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request, send_from_directory, \
    send_file, jsonify
from bokeh.core.properties import value
from bokeh.models import (HoverTool, FactorRange, Plot, LinearAxis, Grid, Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import Figure, show, output_file
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource
import numpy as np
import pandas as pd

# Local application imports
import Magnet.analytics.extractores.claro_mobile_paquetes as claro_paquetes
import Magnet.analytics.extractores.movistar_mobile_paquetes as movistar_paquetes
import Magnet.analytics.extractores.tigoune_mobile_paquetes as tigo_paquetes

import Magnet.analytics.extractores.claro_mobile_pospago as claro_pospago
import Magnet.analytics.extractores.movistar_mobile_pospago as movistar_pospago

import Magnet.analytics.extractores.tigoune_cellphones as tigo_celulares

# Define the blueprint: 'start', set its url prefix: app.url/start
module_start = Blueprint('start', __name__, url_prefix='/')


''' Definiciones de START'''


@module_start.route('/', methods=['GET'])
def index():
    table = claro_paquetes.get_plans()
    return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes Claro")


@module_start.route('/paquetes_claro', methods=['GET'])
def paquetes_claro():
    table = claro_paquetes.get_plans()
    return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes Claro")


@module_start.route('/paquetes_movistar', methods=['GET'])
def paquetes_movistar():
    table = movistar_paquetes.get_plans()
    return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes Movistar")


@module_start.route('/paquetes_tigo', methods=['GET'])
def paquetes_tigo():
    table = tigo_paquetes.get_plans()
    return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes TIGOUNE")


@module_start.route('/pospago_claro', methods=['GET'])
def pospago_claro():
    table = claro_pospago.get_plans()
    return render_template('dashboard/pospago.html', table=table, section_title="Pospago Claro")


@module_start.route('/pospago_movistar', methods=['GET'])
def pospago_movistar():
    table = movistar_pospago.get_plans()
    return render_template('dashboard/pospago.html', table=table, section_title="Pospago Movistar")


@module_start.route('/celulares_tigo', methods=['GET'])
def celulares_tigo():
    table = tigo_celulares.get_cellphones()
    return render_template('dashboard/celulares.html', table=table, section_title="Celulares TIGOUNE")


@module_start.route('/download_data/<file>', methods=['GET'])
def download_data(file):
    try:
        data_directory = os.path.abspath(os.path.join(__file__, "../../../Magnet/analytics/data"))
        file_path = glob.glob(data_directory + '/**/' + '{}.csv'.format(file), recursive=True)
        return send_file(file_path[0], attachment_filename="{}.csv".format(file), as_attachment=True)
    except Exception as e:
        return str(e)


@module_start.route('/data/api/v1.0/<tipo>/<operador>', methods=['GET'])
def get_data(tipo, operador):
    if tipo == "planes" and operador == "claro":
        list = claro_pospago.get_plans()
        return jsonify(results=list)
    elif tipo == "planes" and operador == "movistar":
        list = movistar_pospago.get_plans()
        return jsonify(results=list)
    elif tipo == "paquetes" and operador == "claro":
        list = claro_paquetes.get_plans()
        return jsonify(results=list)
    elif tipo == "paquetes" and operador == "movistar":
        list = movistar_paquetes.get_plans()
        return jsonify(results=list)
    elif tipo == "paquetes" and operador == "tigo":
        list = tigo_paquetes.get_plans()
        return jsonify(results=list)
    elif tipo == "celulares" and operador == "tigo":
        list = tigo_celulares.get_cellphones()
        return jsonify(results=list)
    else:
        return jsonify(results="Sin Resultados")


@module_start.route('/dashboard', methods=['GET'])
def get_dashboard():
    # p = Figure(title='Sine', x_axis_type="datetime", plot_width=1110, plot_height=400)
    # x = np.linspace(-10, 10, 200)
    # y = np.sin(x)
    # p.line(x=x, y=y)
    # script, div = components(p)

    operadores = ['Claro', 'Movistar', 'Tigo-Une']
    categorias = ["Voz", "Datos", "Voz y Datos"]
    colors = ["#c9d9d3", "#718dbf", "#e84d60"]

    data = {'operadores': operadores,
            'Voz': [2, 1, 4],
            'Datos': [5, 3, 4],
            'Voz y Datos': [3, 2, 4]}

    p = Figure(x_range=operadores, plot_width=1400, plot_height=250, title="Tipos de Planes x Operador",
               toolbar_location=None, tools="hover", tooltips="$name @operadores: @$name")

    p.vbar_stack(categorias, x='operadores', width=0.9, color=colors, source=data,
                 legend=[value(x) for x in categorias])

    script, div = components(p)

    return render_template('dashboard/dashboard.html', script=script, div=div, section_title="Dashboard")
