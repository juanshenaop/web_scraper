# -*- coding: utf-8 -*-
from flask import Flask, render_template
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy


# Define the WSGI application object
app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)

# Define the database object which is imported by modules and controllers
db = SQLAlchemy(app)

# Configurations
app.config.from_object('config')


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('errors/404.html'), 404


# Import a module / component using its blueprint handler variable (mod_auth)
from Magnet.mod_start.views import module_start as start_module


# Register blueprint(s)
app.register_blueprint(start_module)


# Build the database
db.create_all()