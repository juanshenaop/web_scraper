# -*- coding: utf-8 -*-
# Standard library imports
import re

# Third party imports

# Local application imports

text = "{'fc_valor': 'Minutos Incluidos a Todo Destino:  Ilim', 'fc_tipo_campo': 'txt', 'fi_orden': '4', 'fi_caracteristica_ws': '70'}, {'fi_plan': 934, 'fi_caracteristica': 245, 'fc_nombre': 'Minutos LDI Incluidos (00444)', 'fc_valor_mostrar': 'DVA', 'fc_valor': 'Minutos LDI Incluidos (00444) 500 Estados Unidos, Canad&#225;, Puerto Rico y M&#233;xico', 'fc_tipo_campo': 'txt', 'fi_orden': '10000', 'fi_caracteristica_ws': '236'}, {'fi_plan': 934, 'fi_caracteristica': 148, 'fc_nombre': 'Valor por Kb Adicional', 'fc_valor_mostrar': 'DVA', 'fc_valor': 'Valor por Kb Adicional 0,036 Impuestos Incluidos', 'fc_tipo_campo': 'txt', 'fi_orden': '10000', 'fi_caracteristica_ws': '172'}, {'fi_plan': 934, 'fi_caracteristica': 29, 'fc_nombre': 'Navegaci&#243;n en Internet', 'fc_valor_mostrar': 'DVA', 'fc_valor': 'Navegaci&#243;n en Internet 2,5GB + 2,5GB promocionales ', 'fc_tipo_campo': 'txt', 'fi_orden': '10000', 'fi_caracteristica_ws': '162'}], 'lstPlanFiltros': [{'fi_plan': 934, 'fi_id_filtro_producto': 69, 'fi_valor': -1}, {'fi_plan': 934, 'fi_id_filtro_producto': 70, 'fi_valor': -5}, {'fi_plan': 934, 'fi_id_filtro_producto': 63, 'fi_valor': 288}, {'fi_plan': 934, 'fi_id_filtro_producto': 71, 'fi_valor': 55900}, {'fi_plan': 934, 'fi_id_filtro_producto': 64, 'fi_valor': 290}], 'fc_detalle': '1|M&aacute;s informaci&oacute;n|DET|_self', 'fc_contratar': '1|Contratar|FRM|_self'}"


""" Componente Extractor de Valores - CLARO """

def get_minutes_claro(text):

    def get_minutes_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")
        minutes = pattern.findall(str(text))
        minutes = list(filter(None, minutes))
        try:
            if len(minutes) == 0:
                return "Ilimitado"
            elif minutes is not None:
                return minutes[0]
        except IndexError:
            pass

    pattern = re.compile(r"(Min[a-zA-Z0-9\s:]*)")

    if pattern.findall(str(text)) is not None:
        minutes_string = pattern.findall(str(text))
        minutes_string = list(filter(None, minutes_string))
        print(minutes_string[0])

        return get_minutes_from_string(minutes_string[0])

print(get_minutes_claro(text))