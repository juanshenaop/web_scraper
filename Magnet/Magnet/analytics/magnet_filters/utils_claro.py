# -*- coding: utf-8 -*-
# Standard library imports
import re

# Third party imports

# Local application imports


""" Componente Extractor de Valores - CLARO """


def get_price(text):
    pattern = re.compile(r"([0-9]{1,9}\.[0-9]{1,9})")
    price = pattern.findall(str(text))
    price = list(filter(None, price))
    return price


def get_data(text):
    def get_data_from_string(text):
        pattern = re.compile(r"([0-9]{0,9}.[0-9]{0,9}\wB)")
        data = pattern.findall(str(text))
        data = list(filter(None, data))
        try:
            if len(data) == 0:
                return "Ilimitado"
            elif data is not None:
                return data[0]
        except IndexError:
            pass
    pattern = re.compile(r"([0-9]{0,9}.[0-9]{0,9}\wB)")
    if pattern.findall(str(text)) is not None:
        data_string = pattern.findall(str(text))
        data_string = list(filter(None, data_string))
        if len(data_string) == 0:
            return "No Tiene Datos"
        elif len(data_string) > 0:
            return get_data_from_string(data_string)


def get_minutes(text):
    def get_minutes_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")
        minutes = pattern.findall(str(text))
        minutes = list(filter(None, minutes))
        try:
            if len(minutes) == 0:
                return "Ilimitado"
            elif minutes is not None:
                return minutes[0]
        except IndexError:
            pass
    pattern = re.compile(r"([a-zA-Z0-9\s:]*Min[a-zA-Z0-9\s:]*)")
    if pattern.findall(str(text)) is not None:
        minutes_string = pattern.findall(str(text))
        minutes_string = list(filter(None, minutes_string))
        if len(minutes_string) == 0:
            return "No Tiene Minutos"
        elif len(minutes_string) > 0:
            return get_minutes_from_string(minutes_string)


def get_sms(text):
    def get_sms_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")
        sms = pattern.findall(str(text))
        sms = list(filter(None, sms))
        try:
            if len(sms) == 0:
                return "Ilimitado"
            elif sms is not None:
                return sms[0]
        except IndexError:
            pass
    pattern = re.compile(r"([a-zA-Z0-9\s:]*SMS[a-zA-Z0-9\s:]*)")
    if pattern.findall(str(text)) is not None:
        sms_string = pattern.findall(str(text))
        sms_string = list(filter(None, sms_string))
        if len(sms_string) == 0:
            return "No Tiene SMS"
        elif len(sms_string) > 0:
            return get_sms_from_string(sms_string)


def get_zero_rating(text):
    pattern = re.compile(r"(Facebook)|(WhatsApp)|(Twitter)")
    socialnetworks_string = pattern.findall(str(text))
    socialnetworks_string = list(filter(None, socialnetworks_string))
    return socialnetworks_string


def get_apps(text):
    pattern = re.compile(r"(Claro_Musica)|(Claro_Video)")
    apps_string = pattern.findall(str(text))
    apps_string = list(filter(None, apps_string))
    return apps_string


def get_life_span(text):
    def get_life_span_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")
        life_span = pattern.findall(str(text))
        life_span = list(filter(None, life_span))
        try:
            if len(life_span) == 0:
                return "Ilimitado"
            elif life_span is not None:
                return life_span[0]
        except IndexError:
            pass
    pattern = re.compile(r"([0-9]{0,9} d)|([0-9]{0,9} hora)")
    if pattern.findall(str(text)) is not None:
        life_span_string = pattern.findall(str(text))
        life_span_string = list(filter(None, life_span_string))
        if len(life_span_string[0][0]) == 0:
            return get_life_span_from_string(life_span_string) + " Hora(s)"
        elif len(life_span_string) > 0:
            return get_life_span_from_string(life_span_string) + " Días"
