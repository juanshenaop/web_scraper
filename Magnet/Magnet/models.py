# -*- coding: utf-8 -*-
import datetime

from flask_login import UserMixin, login_manager
from werkzeug.security import generate_password_hash, check_password_hash

from Magnet.Magnet import db


class User(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    email = db.Column('email', db.String(200), index=True, unique=True)
    password_hash = db.Column('password_hash', db.String(200))
    registered_on = db.Column('registered_on', db.DateTime, default=datetime.datetime.now)
    accept_conditions = db.Column('accept_conditions', db.Boolean, default=False)
    confirmed = db.Column('confirmed', db.Boolean, default=False)
    confirmed_on = db.Column('confirmed_on', db.DateTime, nullable=True)
    admin = db.Column('admin', db.Boolean, default=False)
    permissions = db.Column('permissions', db.Integer, default='4')
    authorized = db.Column('authorized', db.Boolean, default=True)
    name = db.Column('name', db.String(100), nullable=True)
    company = db.Column('company', db.String(100), nullable=True)
    bio = db.Column('bio', db.String(600), nullable=True)

    def __repr__(self):
        return '<User {}>'.format(self.email)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Cafam(UserMixin, db.Model):
    __tablename__ = "cafam"
    id = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    email = db.Column('email', db.String(200), index=True, unique=True)
    city = db.Column('city', db.String(500), index=True)
    level = db.Column('level', db.String(500), index=True)
    position = db.Column('position', db.String(500), index=True)
