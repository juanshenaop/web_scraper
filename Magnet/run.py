# -*- coding: utf-8 -*-
from cheroot.wsgi import Server as WSGIServer

from Magnet import app
#from Magnet.config import PRODUCTION_ENVIROMENT


PRODUCTION_ENVIROMENT = False

if __name__ == '__main__':

    # RUN CherryPY
    if PRODUCTION_ENVIROMENT is True:
        server = WSGIServer(bind_addr=('0.0.0.0', int(5000)), wsgi_app = app, numthreads = 100)

    # RUN Flask without WSGI
    else:
        app.run(host='0.0.0.0', port=5000, debug=True)
        try:
            server.start()
        except KeyboardInterrupt:
            server.stop()
